<?php

/**
 * @file
 * Improved styling of the Drupal admin tabs.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_page_attachments().
 *
 * Add admin tabs css for logged in users only.
 */
function better_admin_tabs_page_attachments(array &$page) {
  if (_enable_better_admin_tabs()) {
    $page['#attached']['library'][] = 'better_admin_tabs/css';
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * Add twig file suggestion for the menu_local_tasks.
 */
function better_admin_tabs_theme_suggestions_menu_local_tasks_alter(array &$suggestions, array $variables) {
  if (_enable_better_admin_tabs()) {
    $suggestions[] = 'menu_local_tasks__better';
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * Add twig file suggestion for the menu_local_task.
 */
function better_admin_tabs_theme_suggestions_menu_local_task_alter(array &$suggestions, array $variables) {
  if (_enable_better_admin_tabs()) {
    $suggestions[] = 'menu_local_task__better';
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * Add twig file suggestion for the menu_local_action.
 */
function better_admin_tabs_theme_suggestions_menu_local_action_alter(array &$suggestions, array $variables) {
  if (_enable_better_admin_tabs()) {
    $suggestions[] = 'menu_local_action__better';
  }
}

/**
 * Implements hook_theme().
 *
 * Use this modules custom twig files for:
 * - menu_local_tasks
 * - menu_local_task
 * - menu_local_action.
 */
function better_admin_tabs_theme() {
  return [
    'menu_local_tasks__better' => [
      'variables' => ['primary' => [], 'secondary' => []],
      'template' => 'menu-local-tasks--better',
    ],
    'menu_local_task__better' => [
      'render element' => 'element',
      'base hook' => 'menu_local_task',
      'template' => 'menu-local-task--better',
    ],
    'menu_local_action__better' => [
      'render element' => 'element',
      'base hook' => 'menu_local_action__better',
      'template' => 'menu-local-action--better',
    ],
  ];
}

/**
 * Implements hook_help().
 *
 * @inheritdoc
 */
function better_admin_tabs_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the better_admin_tabs module.
    case 'help.page.better_admin_tabs':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Make the Drupal admin tabs (create, edit,
      translate, ...) look nice') . '</p>';
      return $output;

    default:
  }
}

/**
 * Helper function to determin wheter or not to activate Better Admin Tabs.
 *
 * Conditions:
 * - user must be logged in.
 * - current page must not be an admin page.
 *
 * @return bool
 *   TRUE or FALSE
 */
function _enable_better_admin_tabs() {

  // Only enable for logged in users.
  if (!\Drupal::currentUser()->isAuthenticated()) {
    return FALSE;
  }

  // Only enable on none-admin pages.
  if (\Drupal::service('router.admin_context')->isAdminRoute()) {
    return FALSE;
  }

  // Otherwise: enable.
  return TRUE;
}
